
__kernel void clCopy(__global const uchar4* src,
                           __global uchar4* dst,
                           uint width, 
                           uint height)
{
  int x = get_global_id(0);
  int y = get_global_id(1);
  if (x >= width || y >= height) return;
  
  int i = y * width + x;	// Linear Iterator

  uchar4 color = src[i];

  dst[i] = (uchar4)(color.x,color.y,color.z, 255);

}

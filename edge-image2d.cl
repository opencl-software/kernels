/**
 * Simple edge-detect algorithm based on pixel-to-contourn
 * difference (no differential computation against any matrix).
 *
 * Singular differences are then multiplicated by the respective
 * luminance equation cohefficients
 *
 *  L(c) = c.r * 0.2126 + c.g * 0.7152 + c.b * 0.0722
 *
 * 2D kernel version
 *
 */

__kernel void 
clEdgeDetect(read_only image2d_t src,
             write_only image2d_t dst,
	     sampler_t sampler,
             float width, 
             float height)
{
  int x = get_global_id(0);
  int y = get_global_id(1);

  int wgx = get_local_size(0);
  int wgy = get_local_size(1);

  if (x >= width || y >= height) return;
  
  int i = y * width + x;	// Pixel Iterator

  int vpixels = 1;
  int hpixels = 1;
  int dpixels = 1;

  int i_max = width * height - hpixels;
  int j_max = width * (height - vpixels);
  int k_max = (width - dpixels ) * (height - dpixels);
  int w_max =  width * (height - dpixels) ;

  if((x < (width - 1)) && 
     (y < (height - 1)))
  {
    float4 colorx = read_imagef(src, sampler, (int2)( x , y));
    float4 color1x = read_imagef(src, sampler, (int2)( x - 1 , y ));
    float4 colorx1 = read_imagef(src, sampler, (int2)( x + 1 , y ));

    float4 colory = read_imagef(src, sampler, (int2)( x , y));
    float4 color1y = read_imagef(src, sampler, (int2)( x , y - 1 ));
    float4 colory1 = read_imagef(src, sampler, (int2)( x , y + 1 ));    

    float4 colorz = read_imagef(src, sampler, (int2)( x , y));
    float4 color1z = read_imagef(src, sampler, (int2)( x - 1, y - 1 ));
    float4 colorz1 = read_imagef(src, sampler, (int2)( x + 1, y + 1 ));    

    float4 colorw = read_imagef(src, sampler, (int2)( x , y));
    float4 color1w = read_imagef(src, sampler, (int2)( x + 1, y - 1 ));
    float4 colorw1 = read_imagef(src, sampler, (int2)( x - 1, y + 1 ));        
  
    float rx = 0;
    float gx = 0;
    float bx = 0;
    float r1x = 0;
    float g1x = 0;
    float b1x = 0;

    float ry = 0;
    float gy = 0;
    float by = 0;
    float r1y = 0;
    float g1y = 0;
    float b1y = 0;
    
    float rz = 0;
    float gz = 0;
    float bz = 0;
    float r1z = 0;
    float g1z = 0;
    float b1z = 0;

    float rw = 0;
    float gw = 0;
    float bw = 0;
    float r1w = 0;
    float g1w = 0;
    float b1w = 0;

    // "Forward"
	
    rx = (colorx1.x - colorx.x)/hpixels * 0.2126f;
    if(rx < 0)
	rx = -rx;

    gx = (colorx1.y - colorx.y)/hpixels * 0.7152f;
    if(gx < 0)
    	gx = -gx;

    bx = (colorx1.z - colorx.z)/hpixels * 0.0722f;
    if(bx < 0)
    	bx = -bx;
         

    ry = (colory1.x - colory.x)/vpixels * 0.2126f;
    if(ry < 0)
	ry = -ry;

    gy = (colory1.y - colory.y)/vpixels * 0.7152f;
    if(gy < 0)
	gy = -gy;

    by = (colory1.z - colory.z)/vpixels * 0.0722f;
    if(by < 0)
	by = -by;
                

    rz = (colorz1.x - colorz.x)/dpixels * 0.2126f;
    if(ry < 0)
	ry = -ry;

    gz = (colorz1.y - colorz.y)/dpixels * 0.7152f;
    if(gy < 0)
	gy = -gy;

    bz = (colorz1.z - colorz.z)/dpixels * 0.0722f;
    if(bz < 0)
	bz = -bz;


    rw = (colorw1.x - colorw.x)/dpixels * 0.2126f;
    if(rw < 0)
    	rw = -rw;

    gw = (colorw1.y - colorw.y)/dpixels * 0.7152f;
    if(gw < 0)
	gw = -gw;

    bw = (colorw1.z - colorw.z)/dpixels * 0.0722f;
    if(bw < 0)
	bw = -bw;

    // "Backward"

    r1x = (colorx.x - color1x.x)/hpixels * 0.2126f;
    if(r1x < 0)
        r1x = -r1x;

    g1x = (colorx.y - color1x.y)/hpixels * 0.7152f;
    if(g1x < 0)
        g1x = -g1x;

    b1x = (colorx.z - color1x.z)/hpixels * 0.0722f;
    if(b1x < 0)
        b1x = -b1x;


    r1y = (colory.x - color1y.x)/vpixels * 0.2126f;
    if(r1y < 0)
        r1y = -r1y;

    g1y = (colory.y - color1y.y)/vpixels * 0.7152f;
    if(g1y < 0)
        g1y = -g1y;

    b1y = (colory.z - color1y.z)/vpixels * 0.0722f;
    if(b1x < 0)
        b1y = -b1y;


    r1z = (colorz.x - color1z.x)/dpixels * 0.2126f;
    if(r1z < 0)
        r1z = -r1z;

    g1z = (colorz.y - color1z.y)/dpixels * 0.7152f;
    if(g1z < 0)
        g1z = -g1z;

    b1z = (colorz.z - color1z.z)/dpixels * 0.0722f;
    if(b1z < 0)
        b1z = -b1z;


    r1w = (colorw.x - color1w.x)/dpixels * 0.2126f;
    if(r1w < 0)
        r1w = -r1w;

    g1w = (colorw.y - color1w.y)/dpixels * 0.7152f;
    if(g1w < 0)
        g1w = -g1w;

    b1w = (colorw.z - color1w.z)/dpixels * 0.0722f;
    if(b1w < 0)
        b1w= -b1w;

    
    write_imagef(dst, (int2)(x,y), (float4) (r1x|r1y|r1z|r1w, gx, b1x|b1y|b1z|b1w, 255));

  }
}


__global uchar4* sharpen_g(__global uchar4* buf, 
                  uchar xvalue, 
                  uchar yvalue, 
                  uchar zvalue,
                  uchar wvalue, 
                  uint pixel, 
                  uint mask)
{
    buf[pixel] = (uchar4)(0, (xvalue & mask) , 0, 255);
    buf[pixel] = (uchar4)(0, buf[pixel].y | (yvalue & mask) , 0, 255);
    buf[pixel] = (uchar4)(0, buf[pixel].y | (zvalue & mask) , 0, 255);
    buf[pixel] = (uchar4)(0, buf[pixel].y | (wvalue & mask) , 0, 255);

    return buf;
}

__kernel void clThresholdBinary(__global const uchar4 *src,
                                __global uchar4 *dst,
                                uint    width,
                                uint    height, uchar threshold)
{
        x = get_global_id(0);
        y = get_global_id(1);

        if(x >= width || y >=height)
                return;

        int i = y * width + x;

        if(src[i] > threshold)
                dst[i]=255;
        else
                dst[i]=0;

} 

__kernel void clThresholdBinaryInverted(__global const uchar4 *src,
                                __global uchar4 *dst,
                                uint    width,
                                uint    height, uchar threshold)
{
        x = get_global_id(0);
        y = get_global_id(1);

        if(x >= width || y >=height)
                return;

        int i = y * width + x;

        if(src[i] > threshold)
                dst[i]=0;
        else
                dst[i]=255;

}

__kernel void clThresholdTruncate(__global const uchar4 *src,
                                __global uchar4 *dst,
                                uint    width,
                                uint    height, uchar threshold)
{
        x = get_global_id(0);
        y = get_global_id(1);

        if(x >= width || y >=height)
                return;

        int i = y * width + x;

        if(src[i] > threshold)
                dst[i]=threshold;
        else
                dst[i]=src[i];

}  

__kernel void clThresholdZero(__global const uchar4 *src,
                                __global uchar4 *dst,
                                uint    width,
                                uint    height, uchar threshold)
{
        x = get_global_id(0);
        y = get_global_id(1);

        if(x >= width || y >=height)
                return;

        int i = y * width + x;

        if(src[i] > threshold)
                dst[i]=src[i];
        else
                dst[i]=0;

} 

__kernel void clThresholdZeroInverted(__global const uchar4 *src,
                                __global uchar *dst,
                                uint    width,
                                uint    height, uchar threshold)
{
        x = get_global_id(0);
        y = get_global_id(1);

        if(x >= width || y >=height)
                return;

        int i = y * width + x;

        if(src[i] > threshold)
                dst[i]=0;
        else
                dst[i]=src[i];

} 


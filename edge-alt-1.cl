/**
 * Simple edge-detect algorithm based on pixel-to-contourn
 * difference (no convolution matrix).
 *
 * Singular differences are then multiplicated by the respective
 * luminance equation cohefficients
 *
 *  L(c) = c.r * 0.2126 + c.g * 0.7152 + c.b * 0.0722
 *
 *  1D (linear buffer) version
 */

#define	arraysize(a)	(sizeof(a)/sizeof(a[0]))

#define	_R	0
#define	_G	1
#define	_B	2
#define _A	3

#define _I 	0
#define _J  	1
#define _K  	2
#define _L  	3

#define	to_char_or(f1,f2,f3,f4)	\
	(			\
		(uchar)f1 |     \
		(uchar)f2 |     \
		(uchar)f3 |     \
		(uchar)f4       \
	)

#define sqr(x) (x*x)

#define	MAX(a, b) ((a > b) ? a : b )

float chan_max(float *diff, int samples)
{
//	if((samples <= 0) || samples > arraysize(diff) )
//		return 0;
	
	float max = diff[0];
	
	for(int i = 0; i < samples; i++)
		max = MAX(max, diff[i]);
	
	return max;
}

__kernel void clEdgeDetect(__global const uchar4* src,
                           __global uchar4* dst,
                           uint width, 
                           uint height)
{
  int x = get_global_id(0);
  int y = get_global_id(1);
  if (x >= width || y >= height) return;
  
  int i = y * width + x;	// Horizontal Iterator
  int j = y * width + x;	// Vertical Iterator
  int k = y * width + x;	// Diagonal Iterator
  int l = k;

  int vpixels = 1;
  int hpixels = 1;
  int dpixels = 1;

  int i_max = width * height - hpixels;
  int j_max = width * (height - vpixels);
  int k_max = (width - dpixels ) * (height - dpixels);
  int l_max =  k_max;

/**
 *    "prev" directions
 *      ______ ______ ______
 *     |      |      |      |
 *     | ik_p | ij_p | iw_p |
 *     |______|______|______|_
 *     |      |        |      |
 *     |  i_p |    i   |  ii  |   ->   
 *     |______|_ ______|______|
 *      |       |      |      | 
 *      |   iw  |   ij |  ik  |  
 *      |_______|______|______|
 *                
 *    /            |           \
 *  |/_            V           _\|
 *
 *             "next" directions
 */

  int mode = 2;

  if((i > 0 && i < i_max) && 
     (j > 0 && j < j_max) && 
     (k > 0 && k < k_max) &&
     (l > 0 && l < l_max)) 
  {
    /* orizontal scan (WE-EW) */
    int i_next = y * width + (x + hpixels);
    int i_prev = y * width + (x - hpixels);

    /* vertical scan (NS-SN) */
    int j_next = (y + vpixels) * width + x;
    int j_prev = (y - vpixels) * width + x; 
    
    /* diagonal scan (NW-SE) */
    int k_next = (y + dpixels ) * width + (x + dpixels);
    int k_prev = (y - dpixels ) * width + (x - dpixels);
    
    /* diagonal scan (NE-SW) */
    int l_next = (y + dpixels ) * width + ( x - dpixels);
    int l_prev = (y - dpixels ) * width + ( x + dpixels);

    int iterators[8] = {
		i_next, 
		i_prev,
		j_next,
		j_prev,
		k_next,
		k_prev,
		l_next,
		l_prev
    };

    /* Pixels resolution scan (H/V/D+/D-) */
    
    float res[4] = {
        hpixels,
        vpixels,
        dpixels,
        dpixels
    };

    /* Luminance color coefficient */
    float coeff[4] = {
	0.2126f,	/* red */
	0.7152f,	/* green */
	0.0722f,	/* blue */
        1               /* alpha doesn't change */
    };

    /* "nexts" rgb array */
    float rgba[4][4];
    float conv;

    /* "prevs" rgb array */
    float _rgba[4][4];
    
    for(int d = 0; d < 4 ; d++ ) {
	rgba[_R][d] = abs(src[i].x - src[ iterators[(d*2)] ].x)/res[d] * coeff[_R];
        _rgba[_R][d] = abs(src[i].x - src[ iterators[(d*2)+1] ].x)/res[d] * coeff[_R];
    }
        
    for(int d = 0; d < 4 ; d++) {
	rgba[_G][d] = abs(src[i].y - src[ iterators[(d*2)] ].y)/res[d] * coeff[_G];
	_rgba[_G][d] = abs(src[i].y - src[ iterators[(d*2)+1] ].y)/res[d] * coeff[_G];
    }

    for(int d = 0; d < 4 ; d++) {
	rgba[_B][d] = abs(src[i].z - src[ iterators[(d*2)] ].z)/dpixels * coeff[_B];
	_rgba[_B][d] = abs(src[i].z - src[ iterators[(d*2)+1] ].z)/dpixels * coeff[_B];
    }
	
    for(int d = 0; d < 4 ; d++) {
        rgba[_A][d] = abs(src[i].w - src[ iterators[(d*2)] ].w)/dpixels * coeff[_A];
        _rgba[_A][d] = abs(src[i].w - src[ iterators[(d*2)+1] ].w)/dpixels * coeff[_A];
    }	
    
    // UPs

    uchar g_max = (uchar)chan_max(rgba[_G], 4);
   
    // DOWNs

    uchar _g_max = (uchar)chan_max(_rgba[_G], 4);

    uchar G = MAX(g,_g);
	
    dst[i] = (uchar4)(0, _g_max, 0, 255);
    
  }
}


/**
 * Simple edge-detect algorithm based on pixel-to-contourn
 * difference (no differential computation against any matrix).
 *
 * Singular differences are then multiplicated by the respective
 * luminance equation cohefficients
 *
 *  L(c) = c.r * 0.2126 + c.g * 0.7152 + c.b * 0.0722
 *
 *  1D (linear buffer) version
 */


__kernel void clEdgeDetect(__global uchar4* src,
                           __global uchar4* dst,
                           uint width, 
                           uint height)
{
  int x = get_global_id(0);
  int y = get_global_id(1);
  if (x >= width || y >= height) return;
  
  int i = y * width + x;	// Horizontal Iterator
  int j = y * width + x;	// Vertical Iterator
  int k = y * width + x;	// Diagonal Iterator
  int w = k;

  int i_next, j_next, k_next;

  int vpixels = 1;
  int hpixels = 1;
  int dpixels = 1;

  int i_max = width * height - hpixels;
  int j_max = width * (height - vpixels);
  int k_max = (width - dpixels ) * (height - dpixels);
  int w_max =  k_max;

/**
 *    "Backward" 
 *	       
 *      ______ ______ ______
 *     |      |      |      |
 *     | ik_p | ij_p | iw_p |
 *     |______|______|______|_
 *     |      |        |      |
 *     |  i_p |    i   |  ii  | 
 *     |______|_ ______|______|
 *      |       |      |      | 
 *      |   iw  |   ij |  ik  |  
 *      |_______|______|______|
 *                
 *
 *             "Forward"
 */

  int mode = 2;

  if((i > 0 && i < i_max) && 
     (j > 0 && j < j_max) && 
     (k > 0 && k < k_max) &&
     (w > 0 && w < w_max)) 
  {
    int i_next = y * width + (x + hpixels);
    int i_prev = i - hpixels;
    uchar4 colorx = src[i];
    uchar4 colorx1 = src[i_next];
    uchar4 color1x = src[i_prev];

    int j_prev = (y - vpixels) * width + x; 
    int j_next = (y + vpixels) * width + x;
    uchar4 colory = src[j];
    uchar4 colory1 = src[j_next];
    uchar4 color1y = src[i_prev];

    int k_prev = (y - dpixels ) * width + (x - dpixels);
    int k_next = (y + dpixels ) * width + (x + dpixels);
    uchar4 colorz = src[k];
    uchar4 colorz1 = src[k_next];
    uchar4 color1z = src[k_prev];

    int w_prev = (y - dpixels ) * width + ( x + dpixels);
    int w_next = (y + dpixels ) * width + ( x - dpixels);
    uchar4 colorw = src[w];
    uchar4 colorw1 = src[w_next];
    uchar4 color1w = src[w_prev];

    uchar diff = 0;
    uchar rx = 0;
    uchar gx = 0;
    uchar bx = 0;
    uchar r1x = 0;
    uchar g1x = 0;
    uchar b1x = 0;

    uchar ry = 0;
    uchar gy = 0;
    uchar by = 0;
    uchar r1y = 0;
    uchar g1y = 0;
    uchar b1y = 0;
    
    uchar rz = 0;
    uchar gz = 0;
    uchar bz = 0;
    uchar r1z = 0;
    uchar g1z = 0;
    uchar b1z = 0;

    uchar rw = 0;
    uchar gw = 0;
    uchar bw = 0;
    uchar r1w = 0;
    uchar g1w = 0;
    uchar b1w = 0;

    rx = (colorx1.x - colorx.x)/hpixels * 0.2126f;
    if(rx < 0)
	rz = -rx;

    gx = (colorx1.y - colorx.y)/hpixels * 0.7152f;
    if(gx < 0)
    	gx = -gx;

    bx = (colorx1.z - colorx.z)/hpixels * 0.0722f;
    if(bx < 0)
    	bx = -bx;
                
    ry = (colory1.x - colory.x)/vpixels * 0.2126f;
    if(ry < 0)
	ry = -ry;

    gy = (colory1.y - colory.y)/vpixels * 0.7152f;
    if(gy < 0)
	gy = -gy;

    by = (colory1.z - colory.z)/vpixels * 0.0722f;
    if(by < 0)
	by = -by;
                

    rz = (colorz1.x - colorz.x)/dpixels * 0.2126f;
    if(ry < 0)
	ry = -ry;

    gz = (colorz1.y - colorz.y)/dpixels * 0.7152f;
    if(gy < 0)
	gy = -gy;

    bz = (colorz1.z - colorz.z)/dpixels * 0.0722f;
    if(bz < 0)
	bz = -bz;


    rw = (colorw1.x - colorw.x)/dpixels * 0.2126f;
    if(rw < 0)
    	rw = -rw;

    gw = (colorw1.y - colorw.y)/dpixels * 0.7152f;
    if(gw < 0)
	gw = -gw;

    bw = (colorw1.z - colorw.z)/dpixels * 0.0722f;
    if(bw < 0)
	bw = -bw;

    // "Backwards"

    r1x = (colorx.x - color1x.x)/hpixels * 0.2126f;
    if(r1x < 0)
        r1x = -r1x;

    g1x = (colorx.y - color1x.y)/hpixels * 0.7152f;
    if(g1x < 0)
        g1x = -g1x;

    b1x = (colorx.z - color1x.z)/hpixels * 0.0722f;
    if(b1x < 0)
        b1x = -b1x;


    r1y = (colory.x - color1y.x)/vpixels * 0.2126f;
    if(r1y < 0)
        r1y = -r1y;

    g1y = (colory.y - color1y.y)/vpixels * 0.7152f;
    if(g1y < 0)
        g1y = -g1y;

    b1y = (colory.z - color1y.z)/vpixels * 0.2126f;
    if(b1x < 0)
        b1y = -b1y;

    r1z = (colorz.x - color1z.x)/dpixels * 0.2126f;
    if(r1z < 0)
        r1z = -r1z;

    g1z = (colorz.y - color1z.y)/dpixels * 0.7152f;
    if(g1z < 0)
        g1z = -g1z;

    b1z = (colorz.z - color1z.z)/dpixels * 0.2126f;
    if(b1z < 0)
        b1z = -b1z;

    r1w = (colorw.x - color1w.x)/dpixels * 0.2126f;
    if(r1w < 0)
        r1w = -r1w;

    g1w = (colorw.y - color1w.y)/dpixels * 0.7152f;
    if(g1w < 0)
        g1w = -g1w;

    b1w = (colorw.z - color1w.z)/dpixels * 0.2126f;
    if(b1w < 0)
        b1w= -b1w;

    dst[i] = (uchar4)(rx|ry|rz|rw, gx|gy|gz|gw , bx|by|bz|bw, 255);
 

    uchar lum  = (uchar)(0.30f * colorx.x + 0.59f * colorx.y + 0.11f * colorx.z);
    uchar lum1 = (uchar)(0.30f * colorx1.x + 0.59f * colorx1.y + 0.11f * colorx1.z);
    
    uchar lum_d = lum1 - lum;
    
    //dst[i] = (uchar4)(lum,lum,lum, 255);

  }
}


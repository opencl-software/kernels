#define NUM_BINS 60

__kernel void clPosterize (__global uchar* dst, 
	                __global const uchar* src, 
              		const int width,
               		const int height)
{
  const int x = get_global_id (0);
  const int y = get_global_id (1);
  int i = y * width + x;
   
  float v = src[i];

  v = v / 255 * NUM_BINS;
  v = (uint) v * 255 / NUM_BINS;

  dst[i] = v;
}

